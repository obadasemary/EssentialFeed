//
//  FeedItem.swift
//  EssentialFeed
//
//  Created by Abdelrahman Mohamed on 23.06.2021.
//

import Foundation

struct FeedItem {

    let id: UUID
    let description: String?
    let location: String?
    let imageURL: URL
}
